@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
<div>
    <h1>Sales create</h1>
    <form action="{{route('salescustomer.store')}}" method="post">
        @csrf
        <input type="text" name="name" placeholder="name">
        <input type="text" name="lastname" placeholder="lastname">
        <input type="text" name="adress" placeholder="adress">
        <input type="text" name="zipcode" placeholder="zipcode">
        <input type="text" name="city" placeholder="city">
        <input type="text" name="phone" placeholder="phone">
        <input type="text" name="password" placeholder="password">
        <input type="hidden" name="role_ID" value="4">
        <input type="text" name="email" placeholder="email">
        <input type="submit" name="submit" value="Maak klant">
    </form>
</div>
@endsection