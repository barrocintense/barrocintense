@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
<div>
    <h1>Sales Index</h1>
    @foreach($users as $user)
    <li><a href="{{route('salescustomer.show', $user->id)}}">{{$user->name}}</a></li>
    @endforeach
    <input type="button" onclick="location.href='{{route('salescustomer.create')}}'" value="Maak klant aan">
</div>
@endsection