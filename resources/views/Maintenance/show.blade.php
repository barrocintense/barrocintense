
@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
    <div>
        <form action="{{route('maintenance.update', $appointment->id)}}" method="post">
            @csrf
            @method('PUT')
            <select name="product_ID" id="">
                @foreach($products as $product)
                    <option name="product_ID" value="{{$product->id}}">{{$product->name}}</option>
                @endforeach
            </select>
            <select name="status_ID" id="">
                @foreach($status as $item)
                    <option value="{{$item->id}}">{{$item->status_description}}</option>
                @endforeach
            </select>

            <input type="number" name="amount" placeholder="aantal">

            <input type="submit" value="Maak Werkbon">
        </form>
        <input type="button" onclick="location.href='{{route('maintenance.index')}}'" value="Terug">
    </div>
@endsection