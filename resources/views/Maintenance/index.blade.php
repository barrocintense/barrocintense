@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
    <div>
    <h1>Worker Maintenance</h1>
    @foreach($appointment as $appointment)
    <li><a href="{{route('maintenance.show', $appointment->id)}}">{{$appointment->appointment_task}}</a></li>
    @endforeach
    </div>
@endsection