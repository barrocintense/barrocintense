@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
<form action="{{route('product.filter')}}" method="post">
  @csrf
  <div class="form-group">
      <input type="text" name="name">
      <input type="submit" name="submitbtn" value="Zoeken">
      <input type="submit" name="submitbtn" value="Clear">
  </div>
   <div class="form-group">
      <input type="radio" value="instock" name="stock"> <label for="instock">Beschikbaar</label> 
      <input type="radio" value="outstock" name="stock"> <label for="outstock">Uitverkocht</label> 
  </div>
  <div class="form-group">
  </div>

</form>
<h1>Products</h1>
<ul>
<ul>
  @isset($products)
  @foreach($products as $product)
    <li> <a href="{{ route('product.show', $product->id ) }}"> {{ $product->name }}</a> @if($product->status_ID == 5) Momenteel leverbaar @endif @if($product->status_ID == 6) Uit voorraad @endif    </li>
  @endforeach
  @endisset
  </ul>
          
</ul>
@endsection

