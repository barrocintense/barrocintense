@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')

<h1>Product {{$product->name}} Bescrijving:</h1> </b>

@if(isset($product->price))
<p>kosten: €{{ $product->price }}</p> </b>
@endif
@if(isset($product->description))
<p>{{ $product->description}}</p> </b>
@endif
@if(isset($product->supplies))
<p>Producten beschikbaar: {{ $product->supplies}}</p> </b>
@endif
@if($product->available)
<p>Product is momenteel leverbaar</p> </b>
@endif
@if(!$product->available)
<p>Product is niet leverbaar</p> </b>
@endif

<form action="{{route('order.store')}}" method="post">
              @csrf
    <input type="hidden" name="product_id" value="{{$product->id}}" >
    <input type="number" name="quantity" value="1">
    <input type="submit" value="Bestellen" class="btn btn-info">
</form>

<!-- <a class="btn btn-primary" href="{{ route('product.edit', $product->id) }}">EDIT</a> -->
@endsection

