@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
    <h1>Edit product</h1>
    <form action="{{ route('product.update', $product->id) }}" method="POST">
    @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">naam</label>
            <input name="name" type="text" value="{{$product->name}}">
        </div>
        <div class="form-group">
            <label for="price">Kosten</label>
            <input name="price" type="number" step="0.01" value="{{$product->price}}">
        </div>
        @if(isset($product->supplies))
        <div class="form-group">
            <label for="supplies">Beschikbaarheid</label>
            <input name="supplies" type="number" value="{{$product->supplies}}">
        </div>
        @endif

        <input type="submit" value="Edit product">
    </form>
        
    @endsection

