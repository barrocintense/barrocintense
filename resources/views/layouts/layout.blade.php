<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    <link href="{{ asset('css/contact.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <title>@yield('title')</title>
</head>
<body>
<header>
    <div class="header-content">
        <div class="logo">
            <img src="{{ asset('img/Logo6_klein.png') }}" alt="">
        </div>
        <div class="nav-menu">
            <div class="link">
                <a href="">Home</a>
            </div>
            @if(Auth::user())
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
                <input type="submit" value="Uitloggen">
            </form>
            <div class="link">
            <a href=""
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                Uitloggen
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            </div>
            @endif
            
            @if(!Auth::user())
            <div class="link">
                <a href="{{ route('login') }}">Inloggen</a>
            </div>
            @endif
        </div>
    </div>
</header>
<main>
    @yield('content')
</main>
<footer>
    <div class="footer-content">
        <h2>&copy; Barroc Intense 2019</h2>
    </div>
</footer>
</body>
</html>