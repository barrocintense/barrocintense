@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
    <div>
    <h1>Contract</h1>
    <form action="{{route('finance.store')}}" method="post">
    @csrf

        <select name="user_ID" id="">
            @foreach($users as $user)

                <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
        <input type="date" name="start_date" placeholder="start date">
        <input type="date" name="end_date" placeholder="start end">
        <select name="lease_type_ID" id="">
            @foreach($lease_types as $lease_type)
                <option value="{{$lease_type->id}}">{{$lease_type->lease_type}}</option>
            @endforeach
        </select>

        <div style="display: flex"><input type="checkbox" name="contract_agreed" value="1"><p>Contract Getekend</p></div>

        <input type="submit" name="submit" value="Maak Contract">
    </form>
    </div>
@endsection