@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
    <div class="main-content">
        <div class="customer-dashboard">
            <div class="titel">
                <h1>Gegevens aanpassen</h1>
            </div>
            <form action="{{route('Customer.update', $user[0]->id)}}" id="edit-info-form" method="post">
                @method('PUT')
                <input name="CompanyDetail" type="hidden" value="{{$user[0]->CompanyDetail->id}}">
                @csrf
                <label for="name">Naam:</label>
                <input name="name" type="text" value="{{$user[0]->name}}">
                <label for="lastname">Achternaam:</label>
                <input name="lastname" type="text" value="{{$user[0]->lastname}}">
                <label for="adress">adres:</label>
                <input name="adress" type="text" value="{{$user[0]->adress}}">
                <label for="zipcode">Postcode:</label>
                <input name="zipcode" type="text" value="{{$user[0]->zipcode}}">
                <label for="city">Stad:</label>
                <input name="city" type="text" value="{{$user[0]->city}}">
                <label for="phone">Mobiel:</label>
                <input name="phone" type="phone" value="{{$user[0]->phone}}">
                <label for="email">Email:</label>
                <input name="email" type="email" value="{{$user[0]->email}}">
                <label for="email">Kvk nummer:</label>
                <input name="kvk_number" type="text" value="{{$user[0]->CompanyDetail->kvk_number}}">
                <label for="email">Iban nummer:</label>
                <input name="iban_number" type="text" value="{{$user[0]->CompanyDetail->iban_number}}">
                <input type="submit">
            </form>
            @endsection
        </div>
    </div>
