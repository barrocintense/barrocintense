@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
    <div class="main-content">
        <div class="customer-dashboard">
            <div class="titel">
                <h1>Geef storing door</h1>
            </div>
            <form action="{{route('Customer.store')}}" id="maintenance" method="post">
                @method('POST')
                @csrf
                <label for="lease">Contract</label>
                <select name="lease" id="">
                    @foreach($leases as $lease)
                    <option value="{{$lease->id}}">{{$lease->id}}</option>
                    @endforeach
                </select>
                <label for="description">Omschrijving probleem</label>
                <textarea name="description" id="maintenance_desc"></textarea>
                <input type="submit">
            </form>
        </div>
    </div>

@endsection