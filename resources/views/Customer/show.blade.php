@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')
    <div class="main-content">
        <div class="customer-dashboard">
            <div class="titel">
                <h1>Klant Dashboard</h1>
            </div>

            <div class="click">
                <input type="button" onclick="location.href='{{route('Customer.create')}}';" value="Storing melden" />
            </div>
            <h2>Klant gegevens:</h2>
            <ul>

                    <li>Voornaam: {{$user[0]->name}}</li>
                    <li>Achternaam: {{$user[0]->lastname}}</li>
                    <li>Adres: {{$user[0]->adress}}</li>
                    <li>Postcode: {{$user[0]->zipcode}}</li>
                    <li>Stad: {{$user[0]->city}}</li>
                    <li>Tel. {{$user[0]->phone}}</li>
                    <li>Email: {{$user[0]->email}}</li>
            </ul>
            <h2>Bedrijfs gegevens:</h2>
            <ul>

                <li>kvk nummer: {{$user[0]->CompanyDetail->kvk_number}}</li>
                <li>Iban nummer: {{$user[0]->CompanyDetail->iban_number}}</li>
            </ul>
            <input type="button" onclick="location.href='{{route('Customer.edit', $user[0]->id)}}';" value="Gegevens aanpassen" />

            <h2>Contracten</h2>
            @foreach($leases as $lease)
                <ul>
                    <li>Klant: {{$user[0]->name}} {{$user[0]->lastname}}</li>
                    <li>Contractnummer: {{$lease->id}}</li>
                    <li>Startdatum: {{$lease->start_date}} </li>
                    <li>Einddatum: {{$lease->end_date}} </li>
                    <li>Lease type: {{$lease->leaseTypes->lease_type}} </li>
                    <li>Contract getekend:
                        @if($lease->contract_agreed == 1)
                            Ja
                        @else
                            Nee
                        @endif
                    </li>
                    <h3>Producten</h3>
                    @foreach($lease->leaseProducts as $product)
                    <li>{{$product->name}}</li>
                        @endforeach

                </ul>
            @endforeach


            <h2>Facturen</h2>
            @foreach($invoices as $invoice)
                <ul>
                    <li>Startdatum: {{$invoice->start_date}}</li>
                    <li>Einddatum: {{$invoice->end_date}}</li>
                    <li>Contractnummer: {{$invoice->lease_ID}}</li>
                    <li>Factuur betaald:
                        @if($invoice->payed == 1)
                            Ja
                        @else
                            Nee
                        @endif
                    </li>
                </ul>
            @endforeach
        </div>
    </div>

@endsection
