@component('mail::message')
Je hebt de volgende product(en) besteld;
Product: {{$product->name}}
@component('mail::button', ['url' => ''])
Log in om uw bestelling te bekijken
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
