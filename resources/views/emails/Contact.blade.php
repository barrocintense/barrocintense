@component('mail::message')
Someone wants to buy your product
@component('mail::button', ['url' => ''])
Maak afspraak
@endcomponent

 Thanks,<br>
{{ config('app.name') }}
@endcomponent