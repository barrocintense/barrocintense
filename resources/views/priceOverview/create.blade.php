@extends('layouts.layout')
@section('title', 'Barroc Intense')
@section('content')

    <div class="main-content">
        <div class="customer-dashboard">
                <form action="{{route('priceOverview.store')}}" method="put">
                    @csrf
                    @foreach($products as $product)
                        <div class="formitem">
                            <label for="{{$product->id}}">{{$product->name}}</label>
                            <input type="number" name="{{$product->name}}" id="{{$product->id}}">
                        </div>
                    @endforeach
                    <input type="submit" value="Vraag prijsopgave aan">
                </form>
        </div>
    </div>

@endsection