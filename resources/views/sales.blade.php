@extends('layouts.layout')
@section('title', 'Barroc Intense')

@section('content')
    <div class="contactForm">
        <form action="" method="post">
            @csrf
            <input type="text" name="firstname" placeholder="firstname">
            <input type="text" name="lastname" placeholder="lastname">
            <input type="text" name="adress" placeholder="adress">
            <input type="text" name="zipcode" placeholder="zipcode">
            <input type="text" name="city" placeholder="city">
            <input type="text" name="phone" placeholder="phone">
            <input type="password" name="password" placeholder="password">
            <input type="password" name="password-repeat" placeholder="password-repeat">
            <select name="role" id="role">
                <option value="1"></option>
            </select>
            <input type="email" name="email" placeholder="email">
            <input type="text" name="kvk_number" placeholder="kvk number">
            <input type="submit" name="submit" value="Create Contact">
        </form>
    </div>

@endsection

