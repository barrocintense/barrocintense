@extends('layouts.layout')
@section('title', 'Barroc Intense')

@section('content')
    <div class="main-content">
        <div class="main-content-centered">

            <div class="item" id="homepage-title">
                <h1>COFFE FACTORY</h1>
            </div>

            <div class="item" id="homepage-img">
                <img src="{{ asset('img/Logo6_groot.png') }}" alt="">
            </div>

            <div class="item" id="homepage-desc">
                <button class="button" onclick="window.location.href = '#';">Producten <i class="fas fa-chevron-right"></i></button>
            </div>

        </div>
    </div>
    <div class="products">
        @for ($i = 0; $i <= 8; $i++)
            <div class="product">
                <div class="product-image">
                    <img src="{{ asset('img/bit-deluxe.png') }}" alt="">
                </div>
                <div class="product-title">
                    <h3>Bit deluxe</h3>
                </div>
                <div class="product-desc">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab amet aspernatur
                        assumenda atque consectetur dicta, dolores eaque facere hic ipsa labore maiores
                        mollitia officia pariatur, provident quas quia voluptatibus! Consequuntur.</p>
                </div>
                <div class="product-button">
                    <button class="button" onclick="window.location.href = '#';">Meer <i class="fas fa-chevron-right"></i></button>
                </div>
            </div>
        @endfor
    </div>
@endsection

