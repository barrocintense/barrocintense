<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class leases extends Model
{
    protected $table = 'leases';

    public function leaseTypes(){
        return $this->hasOne('\App\leaseTypes','id');
    }

    public function leaseRules(){
        return $this->hasMany('\App\LeaseRules','lease_ID');
    }

    public function leaseProducts(){
        return $this->hasMany('\App\product','id');
    }
}
