<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyDetail extends Model
{
    protected $table = 'company_details';

    protected $fillable = ['kvk_number', 'iban_number'];
}
