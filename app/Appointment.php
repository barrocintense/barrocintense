<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    public $table = "Appointment";

    public function lease(){
        return $this->hasOne('\App\Lease', 'id');
    }

    public function status(){
        return $this->hasOne('\App\Status', 'id');
    }
}
