<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = "products";

    protected $fillable = ['name', 'lease_costs', 'connect_costs', 'costs', 'description'];

    public function products()
    {
        //return $this->BelongsTo('\App\product');
    }
}
