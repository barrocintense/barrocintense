<?php

namespace App\Http\Middleware;

use Closure;

class financeCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset(auth()->user()->role_ID))
        {
            abort(403, 'Unauthorized action.');
        }  
        if(auth()->user()->role_ID != 1){
            if(auth()->user()->role_ID != 6){
                abort(403, 'Unauthorized action.');
            }
        }       

        return $next($request);
    }
}
