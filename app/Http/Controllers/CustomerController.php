<?php


namespace App\Http\Controllers;

use App\CompanyDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Appointment;



class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = auth::id();

        $leases = \App\leases::where('user_ID', '=', $id)->get();

        return view('Customer/create', ['leases' => $leases]);    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        Appointment::insert([
            'lease_ID'              => $request->lease,
            'status_ID'             => 3,
            'appointment_task'      => 1,
            'description'           => $request->description
        ]);

        $id = auth::id();


        $user = \App\User::with('companyDetail')->where('id', '=', $id)->get();

        $leases = \App\leases::with('leaseTypes', 'LeaseRules', 'leaseProducts')->where('user_ID', '=', $id)->get();

        $invoices = \App\invoices::with('leases')->where('user_ID', '=', $id)->get();

        return view('Customer/show', ['user' => $user, 'leases' => $leases, 'invoices' => $invoices]);
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userid = auth::id();

        if($id != $userid)
        {
            abort(403, 'Unauthorized action.');
        }

        $user = \App\User::with('companyDetail')->where('id', '=', $id)->get();

        $leases = \App\leases::with('leaseTypes', 'LeaseRules', 'leaseProducts')->where('user_ID', '=', $id)->get();

        $invoices = \App\invoices::with('leases')->where('user_ID', '=', $id)->get();

        return view('Customer/show', ['user' => $user, 'leases' => $leases, 'invoices' => $invoices]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userid = auth::id();

        if($id != $userid)
        {
            abort(403, 'Unauthorized action.');
        }

        $user = \App\User::with('companyDetail')->where('id', '=', $id)->get();


        return view('Customer/edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update([
            'name'           => $request->name,
            'lastname'       => $request->lastname,
            'adress'         => $request->adress,
            'zipcode'        => $request->zipcode,
            'city'           => $request->city,
            'phone'          => $request->phone,
            'email'          => $request->email


        ]);

        $companyID = $request->CompanyDetail;

        $company = CompanyDetail::find($companyID);
        $company->update([
            'kvk_number'     => $request->kvk_number,
            'iban_number'    => $request->iban_number,
        ]);


        return redirect()->route('Customer.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
