<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Product;
use App\Status;
use App\Work_Order;
use App\WorkOrder;
use Illuminate\Http\Request;

class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointment = Appointment::all();
        return view('maintenance/index',['appointment'=>$appointment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        WorkOrder::insert([
           'appointment_ID'     => $request->appointment_ID,
            'product_ID'        => $request->product_ID
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = \App\Appointment::find($id);
        $status = \App\Status::all();
        $products = \App\product::all();

        return view('maintenance/show', ['appointment' => $appointment,'status' => $status,'products' => $products]);
//        $appointment = Appointment::find($id);
//
//        $products = Product::find($id);
//        $products = Product::All();
//
//        $status = Status::find($id);
//        $status = Status::All();

        //dd($products);
        //dd($status);

        //return view('Maintenance/show')->with('appointment', $appointment)->with('products', $products)->with('status', $status)->with('appointmentId', $id);

        // return view('Maintenance/show',['appointment'=>$appointment, 'products' => $products, 'status' => $status]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::find($id);

        $products = Product::find($id);
        return view('maintenance/show',['status' => $status, 'products' => $products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        WorkOrder::insert([
            'appointment_ID' => $id,
            'product_ID'     => $request->product_ID,
            'status_ID'      => $request->status_ID,
            'amount'         => $request->amount
        ]);

        $appointment = \App\Appointment::find($id);
        $status = \App\Status::all();
        $products = \App\product::all();

        return view('maintenance/show', ['appointment' => $appointment,'status' => $status,'products' => $products]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
