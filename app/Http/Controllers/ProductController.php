<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Contact/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = product::find($id);

        return view('marketing/show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Product $product)
    {
               
        return view('marketing.edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update([
            'name'           => $request->name,
            'supplies'       => $request->supplies,
            'price'          => $request->price,
            'available'      => $request->available
        ]);
        return redirect()->route('product.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function filter(Request $request)
    {
        $btn = $_POST['submitbtn'];
        if($btn == "clear"){
            $supplies = Product::where('category_ID', '=', '1')->get();
            return view('marketing.index', ['products' => $supplies]);
        }

        $name = $request->input('name');

        
        $supplies = Product::where('name', 'like', '%'.$name.'%')
        ->where('category_ID', '=',  1)      
        ->get();
        $checkbox_stock = $request->input('instock', false);
        // if($checkbox_stock == 'outstock'){

            if($request['stock'] == 'outstock'){
            $supplies = Product::where('name', 'like', '%'.$name.'%')
            ->where('supplies', '=', 0)
            ->where('category_ID', '=',  1)
            ->get();

        }
            if($request['stock'] == "instock"){
            $supplies = Product::where('name', 'like', '%'.$name.'%')
            ->where('category_ID', '=',  1)
            ->where('supplies', '!=', 0)
            
            ->get();

        }
        return view('marketing.index', ['products' => $supplies]);

    }
}
