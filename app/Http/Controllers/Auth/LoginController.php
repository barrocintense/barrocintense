<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */   

    protected function authenticated($user)
    {
        if(auth()->user()->role_ID == 1){
            return redirect('admin');
        }
        if(auth()->user()->role_ID == 2){
            return redirect('maintenance');
        }
        if(auth()->user()->role_ID == 3){
            return redirect('sales');
        }
        if(auth()->user()->role_ID == 4){

            return redirect('Customer/'.auth()->user()->id);
        }
        if(auth()->user()->role_ID == 5){
            return redirect('headmaintenance');
        }
        if(auth()->user()->role_ID == 6){
            return redirect('finance');
        }
        if(auth()->user()->role_ID == 7){
            return redirect('marketing');
        }
    return redirect('/home');
}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
