<?php

namespace App\Http\Controllers;

use App\Finance;
use App\Lease;
use Illuminate\Http\Request;

class financeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('finance/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = \App\User::all();
        $lease_types = \App\LeaseTypes::all();
        return view('finance/create', ['users' => $users, 'lease_types' => $lease_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checked = 0;

        if ($request->contract_agreed != null){
            $checked = 1;
        }

        Lease::insert([
            'user_ID'           => $request->user_ID,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
            'lease_type_ID'     => $request->lease_type_ID,
            'contract_agreed'   => $checked
        ]);

        return view('finance.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
