<?php

namespace App\Http\Controllers;

use App\cr;
use App\Overview;
use \App\product;
use Illuminate\Http\Request;

class priceOverview extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('priceOverview.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
    $ *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = product::all();
        return view('priceOverview/create' ,['products' => $products]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $overview = new \App\Overview();
        // $overview->
        //dd($request->Barroc_Intens_Italian_Light);


       return ( new \App\Mail\priceOverview($request))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function show(cr $cr)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function edit(cr $cr)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cr $cr)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function destroy(cr $cr)
    {
        //
    }
}
