<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('lease_ID');
            $table->unsignedBigInteger('status_ID');
            $table->unsignedBigInteger('appointment_task');
            $table->text('description');



            $table->foreign('lease_ID')
                ->references('id')
                ->on('leases');

            $table->foreign('status_ID')
                ->references('id')
                ->on('status');

            $table->foreign('appointment_task')
                ->references('id')
                ->on('status');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment');
    }
}
