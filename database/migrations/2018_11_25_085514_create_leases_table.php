<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_ID');
            $table->date('start_date');
            $table->date('end_date');
            $table->unsignedBigInteger('lease_type_ID');
            $table->integer('contract_agreed');

            $table->foreign('user_ID')
                ->references('id')
                ->on('users');

            $table->foreign('lease_type_ID')
                ->references('id')
                ->on('lease_types');




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leases');
    }
}
