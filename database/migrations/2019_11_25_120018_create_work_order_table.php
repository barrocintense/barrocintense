<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('appointment_ID');
            $table->unsignedBigInteger('product_ID');
            $table->unsignedBigInteger('status_ID');
            $table->integer('amount');
            $table->timestamps();

            $table->foreign('appointment_ID')
                ->references('id')
                ->on('appointment');

            $table->foreign('product_ID')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_order');
    }
}
