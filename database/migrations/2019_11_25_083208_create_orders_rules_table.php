<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_ID');
            $table->unsignedBigInteger('product_ID');
            $table->timestamps();

            $table->foreign('order_ID')
                ->references('id')
                ->on('orders');

            $table->foreign('product_ID')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_rules');
    }
}
