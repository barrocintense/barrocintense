<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaseRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lease_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('lease_ID');
            $table->unsignedBigInteger('product_ID');
            $table->timestamps();

            $table->foreign('lease_ID')
                ->references('id')
                ->on('leases');

            $table->foreign('product_ID')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lease_rules');
    }
}
