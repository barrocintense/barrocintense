<?php

use Illuminate\Database\Seeder;

class LeaseRulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lease_rules')->insert([
            [   'lease_ID' => 1,
                'product_ID' => 1,
            ],
            [   'lease_ID' => 1,
                'product_ID' => 2,
            ]
        ]);
    }
}
