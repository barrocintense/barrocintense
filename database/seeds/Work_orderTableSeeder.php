<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class Work_orderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('work_orders')->insert([
            [   'appointment_ID' => '1',
                'product_ID' => '3',
                'status_ID' => '1',
                'amount' => '2',
            ]
        ]);
    }
}
