<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('status')->insert([
            [   'status_description' => 'Storing'
            ],
            [   'status_description' => 'Routine'
            ],
            [   'status_description' => 'Open'
            ],
            [   'status_description' => 'Bezig'
            ],
            [   'status_description' => 'Klaar'
            ],
            [   'status_description' => 'Momenteel leverbaar'
            ],
            [   'status_description' => 'Uit voorraad'
            ],
        ]);
    }
}
