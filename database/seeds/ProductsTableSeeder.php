<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $none = null;
        \App\Product::insert([
            'name' => 'Rubber (10mm)',
            'price' => 00.39,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Rubber (10mm)',
            'price' => 00.45,
            'supplies' => 0,
            'category_ID' => 1,
            'available' => false
        ]);

        \App\Product::insert([
            'name' => 'slang',
            'price' => 4.45,
            'supplies' => 10,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'voeding',
            'price' => 68.69,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Ontkalker',
            'price' => 4,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Waterfilter',
            'price' => 299.45,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Reservoir sensor',
            'price' => 89.99,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Druppelstop',
            'price' =>  122.43,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Electrische pomp',
            'price' => 478.59,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Tandwiel 110mm',
            'price' => 5.45,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Tandwiel 70mm',
            'price' => 5.25,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Maalmotor',
            'price' => 119.20,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'zeef',
            'price' => 28.80,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Reinigingstabletten ',
            'price' => 3.45,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Reiningsborsteltjes',
            'price' => 8.45,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \App\Product::insert([
            'name' => 'Ontkalkingspijp',
            'price' => 21.70,
            'supplies' => 100,
            'category_ID' => 1,
            'available' => true
        ]);

        \DB::table('products')->insert([
            [   'name' => 'Barroc Intens Italian Light',
                'lease_costs' => '499',
                'supplies' => 100,
                'category_ID' => 2,
                'connect_costs' => '289',
                'price' => $none,
                'available' => true
            ],
            [   'name' => 'Barroc Intens Italian',
                'lease_costs' => '599',
                'supplies' => 100,
                'category_ID' => 2,
                'connect_costs' => '289',
                'price' => $none,
                'available' => true
            ],
            [   'name' => 'Barroc Intens Italian Deluxe ',
                'lease_costs' => '799',
                'supplies' => 100,
                'category_ID' => 2,
                'price' => $none,
                'connect_costs' => '375',
                'available' => true
            ],
            [   'name' => 'Barroc Intens Italian Deluxe Special',
                'lease_costs' => '999',
                'supplies' => 100,
                'category_ID' => 2,
                'connect_costs' => '375',
                'price' => $none,
                'available' => true
            ],
            [   'name' => 'Espresso Beneficio',
                'lease_costs' => $none,
                'supplies' => 100,
                'category_ID' => 2,
                'connect_costs' => $none,
                'price' => '21.60',
                'available' => true
            ],
            [   'name' => 'Yellow Bourbon Brasil',
                'lease_costs' => $none,
                'supplies' => 100,
                'category_ID' => 2,
                'connect_costs' => $none,
                'price' => '23.20',
                'available' => true
            ],
            [   'name' => 'Espresso Roma',
                'lease_costs' => $none,
                'supplies' => 100,
                'category_ID' => 2,
                'connect_costs' => $none,
                'price' => '20.80',
                'available' => true
            ],
            [   'name' => 'Red Honey Honduras',
                'lease_costs' => $none,
                'supplies' => 100,
                'category_ID' => 2,
                'connect_costs' => $none,
                'price' => '27.80',
                'available' => true
            ],
        ]);
    }
}
