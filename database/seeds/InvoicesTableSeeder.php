<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('invoices')->insert([
            [   'start_date' => '2004-11-29',
                'end_date' => '2004-12-29',
                'end_date' => '2018-11-29',
                'user_ID' => '4',
                'lease_ID' => '1',
                'payed' => '0'
            ]
        ]);
    }
}
