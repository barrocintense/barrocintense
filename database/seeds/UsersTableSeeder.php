<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            [   'name' => 'Admin',
                'lastname' => 'Admin',
                'adress' => 'kerkstraat 2',
                'zipcode' => '4812BS',
                'city' => 'Breda',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 1,
                'email' => 'admin@barroc.nl'
            ],
            [   'name' => 'Maintenance',
                'lastname' => 'Admin',
                'adress' => 'kerkstraat 2',
                'zipcode' => '4812BS',
                'city' => 'Breda',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 2,
                'email' => 'maintenance@barroc.nl'
            ],
            [   'name' => 'Sales',
                'lastname' => 'Admin',
                'adress' => 'kerkstraat 2',
                'zipcode' => '4812BS',
                'city' => 'Breda',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 3,
                'email' => 'sales@barroc.nl'
            ],
            [   'name' => 'Customer',
                'lastname' => 'Admin',
                'adress' => 'kerkstraat 2',
                'zipcode' => '4812BS',
                'city' => 'Breda',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 4,
                'email' => 'customer@barroc.nl'
            ],
            [   'name' => 'Maintenance-Admin',
                'lastname' => 'Admin',
                'adress' => 'kerkstraat 2',
                'zipcode' => '4812BS',
                'city' => 'Breda',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 5,
                'email' => 'maintenance-admin@barroc.nl'
            ],
            [   'name' => 'Finance',
                'lastname' => 'Admin',
                'adress' => 'kerkstraat 2',
                'zipcode' => '4812BS',
                'city' => 'Breda',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 6,
                'email' => 'finance@barroc.nl'
            ],
            [   'name' => 'Marketing',
                'lastname' => 'Admin',
                'adress' => 'kerkstraat 2',
                'zipcode' => '4812BS',
                'city' => 'Breda',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 7,
                'email' => 'marketing@barroc.nl'
            ],
            [   'name' => 'Gers',
                'lastname' => 'Pardoel',
                'adress' => 'koningsweg 12',
                'zipcode' => '1202LF',
                'city' => 'Amsterdam',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 4,
                'email' => 'gerspardoel@barroc.nl'
            ],
            [   'name' => 'Linda',
                'lastname' => 'De Mol',
                'adress' => 'gaasstraat 13',
                'zipcode' => '1112SF',
                'city' => 'Made',
                'phone' => '06-12345678',
                'password' => Hash::make('Admin'),
                'role_id'    => 4,
                'email' => 'lindademol@barroc.nl'
            ],
        ]);

        $faker = Faker::create();
        for ($i=0; $i <= 150; $i++) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'lastname' => $faker->name,
                'adress' => $faker->name,
                'zipcode' => $faker->name,
                'city' => $faker->name,
                'phone' => $faker->phoneNumber,
                'password' => bcrypt('secret'),
                'role_id' => 4,
                'email' => $faker->email

            ]);
        }
    }
}
