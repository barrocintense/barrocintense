<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class Company_detailsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('company_details')->insert([
            [   'kvk_number' => 18043113,
                'iban_number'    => 'NL05 RABO 1234 1234 00',
                'bkr_verified_at' => '2019-09-29',
                'user_ID' => '4'
            ]
        ]);
    }
}
