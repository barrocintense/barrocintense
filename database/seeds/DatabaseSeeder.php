<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(Lease_typesTableSeeder::class);
        $this->call(LeasesTableSeeder::class);
        $this->call(AppointmentTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(Company_detailsTableSeeder::class);
        $this->call(Customer_notesTableSeeder::class);
        $this->call(LeaseRulesTableSeeder::class);
        $this->call(InvoicesTableSeeder::class);
        $this->call(Work_orderTableSeeder::class);





    }
}
