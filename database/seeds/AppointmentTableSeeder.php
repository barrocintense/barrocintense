<?php

use Illuminate\Database\Seeder;

class AppointmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('appointment')->insert([
            [   'lease_ID' => '1',
                'status_ID' => '3',
                'appointment_task' => '1',
                'description' => 'Koffie automaat deluxe werkt niet meer NEE!!!',
            ],
            [   'lease_ID' => '1',
                'status_ID' => '3',
                'appointment_task' => '2',
                'description' => 'Routine bezoek koffie automaat deluxe, verwacht geen problemen',
            ]
        ]);
    }
}
