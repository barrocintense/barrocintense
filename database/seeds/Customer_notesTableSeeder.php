<?php

use Illuminate\Database\Seeder;

class Customer_notesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('customer_notes')->insert([
            [   'user_ID' => 1,
                'note'    => 'Klant wilt graag ook een koffiebonen contract kijken naar mogelijkheden',
            ],
            [   'user_ID' => 2,
                'note'    => 'Klant heeft een dikke jonko gesmoked',
            ]
        ]);
    }
}
