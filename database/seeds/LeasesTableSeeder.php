<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class LeasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('leases')->insert([
            [   'user_ID' => '4',
                'start_date' => '2004-11-29',
                'end_date' => '2018-11-29',
                'lease_type_ID' => '1',
                'contract_agreed' => '1'
            ]

        ]);
    }
}
