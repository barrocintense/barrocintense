<?php

use Illuminate\Database\Seeder;

class Lease_typesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lease_types')->insert([
            [   'lease_type' => 'Maandelijks'
            ],
            [   'lease_type' => 'Jaarlijks'
        ]
        ]);
    }
}
