<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('marketing/filter', 'productController@filter')->name('product.filter');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('contact', 'ContactController');

Route::resource('admin', 'AdminController')->middleware('adminCheck');

Route::resource('maintenance', 'MaintenanceController')->middleware('maintenanceCheck');

Route::resource('priceOverview', 'priceOverview')->middleware('customerCheck');

Route::resource('customer' , 'CustomerController')->middleware('customerCheck');

Route::resource('headmaintenance', 'HeadMaintenanceController')->middleware('headMaintenanceCheck');

Route::resource('product', 'ProductController')->middleware('marketingCheck');

Route::resource('marketing', 'MarketingController')->middleware('marketingCheck');

Route::resource('order', 'OrderController');

Route::resource('report', 'ReportController');

Route::resource('finance', 'financeController');

Route::resource('salescustomer', 'SalesController');

Route::get('index', function () {
    return view('index');
});

Route::get('sales', function () {
    return view('sales');
});
